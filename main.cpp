#include <iostream>
#include <cstdlib>
#include <QDirIterator>
#include <QFileInfo>
#include <QString>
#include <QDateTime>

int main(int argc, char **argv)
{
    
    if (argc < 3) {
        std::cout << "Insufficient parameters given" << std::endl;
        return 1;
    }
    if (strtol(argv[2],NULL, 10)<=1) {
        std::cout << "Invalid number given" << std::endl;
        return 1;
    }
    QDirIterator dirIterator(
        argv[1],
        QDirIterator::Subdirectories
    );
  
   	long int szam=strtol(argv[2],NULL, 10);
   
	QString act_dir="";
	while (dirIterator.hasNext() && szam>0) {
	dirIterator.next();	
	
	if(dirIterator.fileInfo().path()!=act_dir){
		std::cout<<"Directory: "<<dirIterator.fileInfo().absolutePath().toStdString()<<std::endl;
		act_dir=dirIterator.fileInfo().path();

	}
	else{

	       	
       
	QDateTime datum_seged = dirIterator.fileInfo().created();
        QString datum = datum_seged.toString("yyyy-mm-dd hh:mm");
        datum=datum.leftJustified(30,' ');


        std::cout <<"\t"<< dirIterator.fileInfo().size()<<" "<<datum.toStdString()<<dirIterator.fileInfo().fileName().toStdString()<<std::endl;     

	szam--;
	}
    }

    return 0;
}
